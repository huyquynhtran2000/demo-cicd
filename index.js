const express = require('express');
const app = express();


app.get('/', (req,res) => {
  res.send("Homepage");
})

app.get('/product', (req,res) => {
  res.send("Products page")
})

app.get('/users', (req,res) => {
  res.send("Users page")
})

app.get('/store', (req,res) => {
  res.send("Stores page")
})


const PORT = process.env.PORT || 8080;
app.listen(PORT, () => {
  console.log(`App listening on port ${PORT}`);
});